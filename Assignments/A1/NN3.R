rm(list=ls())
# ============================================================================================= #

sigmoid = function(v) {
  1 / (1 + exp(-v))
}

sigmoid_derivative = function(v) {
  sigmoid(v) * (1 - sigmoid(v))
}

# ============================================================================================= #

mse = function(real_y, computed_y) {
  
}

# ============================================================================================= #

library(ISLR)
data(Auto)

# ============================================================================================= #

set.seed(21041986)
training_size = nrow(Auto) / 2
# train = sample(nrow(Auto), training_size)
train = sample(nrow(Auto), training_size)

attach(Auto)
attributes = cbind(horsepower, weight)

maxs <- apply(attributes, 2, max) 
mins <- apply(attributes, 2, min)
attributes <- as.matrix(scale(attributes, center = mins, scale = maxs - mins))

labels = cbind(mpg)
maxs <- apply(labels, 2, max) 
mins <- apply(labels, 2, min)
labels <- as.matrix(scale(labels, center = mins, scale = maxs - mins))

train.X = attributes[train, ]
train.X = cbind(train.X, rep(1, training_size))
train.Y = labels[train, ]

# ============================================================================================= #

lr = 0.1
p = 2
M = 5
K = 3
rand_numbers = seq(from = -0.7, to = 0.7, by = 0.001)
wi = matrix(sample(rand_numbers, M * (p + 1)), nrow = p + 1, ncol = M)
wo = matrix(sample(rand_numbers, 1 * (M + 1)), nrow = M + 1, ncol = 1)
O = c()
neta = 0.03

# ============================================================================================= #
for(i in 1:100000) {
  Yh = sigmoid(train.X %*% wi) 
  Yo = sigmoid(cbind(Yh, rep(1, training_size)) %*% wo) 
  Eo = train.Y - Yo
  sme = 0.5 * sum(Eo ^ 2)
  print(sme)
  gradient_o = Eo * sigmoid_derivative(Yo)
  delta_o = t(cbind(Yh, rep(1, training_size))) %*% gradient_o
  wo = wo + (neta * delta_o)
  Eh = gradient_o %*% t(wo[1:(nrow(wo) -1), ])
  gradient_h = Eh * sigmoid_derivative(Yh)
  delta_h = t(cbind(Yh, rep(1, training_size))) %*% gradient_h
  wi = wi + (neta * delta_h[1:nrow(wi), 1:ncol(wi)])
}
